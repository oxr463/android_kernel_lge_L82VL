/* Copyright (c) 2013, Code Aurora Forum. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include "../msm8916-lge-common/dsi-panel-lgd-m2db7400-720p-video.dtsi"

&mdss_mdp {
       qcom,mdss-pref-prim-intf = "dsi";
       qcom,mdss-ab-factor = <1 1>;
       qcom,mdss-ib-factor = <2 1>;
};

&dsi_lgd_m2db7400_hd_incell_vid {
       qcom,mdss-dsi-bl-pmic-control-type = "bl_ctrl_wled";
};

&pmx_mdss {
	qcom,num-grp-pins = <3>;
	qcom,pins = <&gp 25>, <&gp 97>, <&gp 110>;
};

&mdss_dsi0 {
       qcom,dsi-pref-prim-pan = <&dsi_lgd_m2db7400_hd_incell_vid>;
       pinctrl-names = "mdss_default", "mdss_sleep";
       pinctrl-0 = <&mdss_dsi_active>;
       pinctrl-1 = <&mdss_dsi_suspend>;

       qcom,platform-dsv_p_en-gpio = <&msm_gpio 110 0>;
       qcom,platform-dsv_n_en-gpio = <&msm_gpio 97 0>;
	   /delete-property/ qcom,platform-bklight-en-gpio;
       qcom,platform-reset-gpio = <&msm_gpio 25 0>;
       qcom,regulator-ldo-mode;
       qcom,platform-regulator-settings = [02 09 03 00 20 00 01];

};

&i2c_4 {
	lm3630@38 {
		compatible = "backlight,lm3630";
		status = "ok";
		revision = "...rev_a";
		reg = <0x38>;
		lm3630,lcd_bl_en = <&msm_gpio 98 0x00>;
		lm3630,max_current = <0x18>;
		lm3630,min_brightness = <0x04>;
		lm3630,default_brightness = <0xA8>;
		lm3630,max_brightness = <0xFF>;
		lm3630,enable_pwm = <0>;
		lm3630,blmap_size = <256>;
		lm3630,blmap = <
			3 3 3 3 3 3 3 3 3 3 3 3 3 3 3
			4 4 4 4 4 4 4 4 4 4 5 5 5 5 5
			5 5 5 5 5 6 6 6 6 6 6 6 6 7 7
			7 8 8 8 9 9 9 9 9 10 10 10 11 11 11
			12 12 12 12 13 13 13 13 14 14 15 15 15 16 16
			17 17 17 18 18 18 19 19 20 21 22 22 23 24 24
			25 25 26 26 27 28 28 29 29 30 30 31 31 32 32
			33 34 35 35 36 36 37 38 39 39 40 41 41 42 43
			44 45 45 46 47 48 49 50 51 52 53 54 55 56 57
			58 59 60 61 62 63 64 65 66 67 68 69 70 71 72
			73 74 75 76 76 77 78 80 81 82 85 89 93 93 93
			94 94 95 95 95 96 97 99 100 102 103 104 106 107 108
			109 110 112 114 115 117 119 121 123 125 127 128 129 130 132
			133 135 136 138 139 140 142 144 146 148 150 151 153 154 156
			157 158 159 161 163 164 165 167 168 170 173 175 177 180 184
			186 188 191 194 197 199 201 203 205 207 209 211 213 215 217
			219 221 223 225 227 228 230 232 235 238 240 243 244 246 248
			249>;
	};
	sm5306@38 {
		compatible = "backlight,sm5306";
		status = "ok";
		revision = "rev_b...";
		reg = <0x38>;
		sm5306,lcd_bl_en = <&msm_gpio 98 0x00>;
		sm5306,max_current = <0x18>;
		sm5306,min_brightness = <0x04>;
		sm5306,default_brightness = <0xA8>;
		sm5306,max_brightness = <0xFF>;
		sm5306,enable_pwm = <0>;
		sm5306,blmap_size = <256>;
		sm5306,blmap = <
			3 3 3 3 3 3 3 3 3 3 4 4 4 4 4 4
			4 4 4 4 4 4 4 4 4 5 5 5 5 5 5 5
			5 5 6 6 6 6 6 6 7 7 7 7 7 8 8 8
			8 8 9 9 9 10 10 10 10 11 11 11 11 11 12 13
			13 13 14 14 14 15 15 16 16 17 17 17 18 18 19 19
			20 20 21 21 22 22 23 23 24 24 25 25 26 27 27 28
			28 29 30 31 32 32 33 34 34 35 36 36 37 37 37 39
			40 41 41 42 43 44 44 45 46 47 48 48 49 50 51 51
			52 53 54 54 55 56 57 58 59 60 61 62 62 63 63 63
			64 65 66 67 68 70 71 72 73 74 75 76 78 79 79 79
			82 84 91 91 91 92 93 94 95 96 97 98 99 99 100 102
			103 105 106 107 109 110 112 113 115 116 118 119 121 122 124 125
			127 128 130 132 133 135 137 138 140 142 143 145 147 148 150 150
			150 155 157 159 161 163 165 167 168 170 172 174 176 178 180 182
			184 186 188 190 192 194 196 198 201 203 205 207 209 211 214 216
			218 220 223 225 227 230 232 234 237 240 243 245 248 250 253 255>;
	};
	sm5107@3e {
		compatible = "sm,dsv-sm5107";
		status = "ok";
		reg = <0x3e>;
	};
};
