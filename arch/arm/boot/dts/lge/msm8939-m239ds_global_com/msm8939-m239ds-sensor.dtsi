/* Copyright (c) 2012, Code Aurora Forum. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

&soc {
i2c@78b6000 { /* BLSP1 QUP2 */
		mpu6050@68 { /* Gyroscope and accelerometer sensor combo */
			compatible = "invn,mpu6050";
			status = "disable";
		};

		avago@39 { /* Ambient light and proximity sensor */
			compatible = "avago,apds9900";
			status = "disable";
		};

		akm@c {
			compatible = "ak,ak8963";
			status = "disable";
		};
	};
};

&i2c_0{
	partron@53 { /* Ambient light and proximity sensor */
		compatible = "partron,pas230";
		reg = <0x53>;
		status = "ok";
		pinctrl-names = "default","sleep";
		pinctrl-0 = <&pas230_int_active>;
		pinctrl-1 = <&pas230_int_suspend>;
		interrupt-parent = <&msm_gpio>;
		interrupts = <113 0x2002>;
		Partron,vdd_ana-supply = <&pm8916_l17_sns>;
		Partron,vddio_i2c-supply = <&pm8916_l6>;
		Partron,irq-gpio = <&msm_gpio 113 0x2002>;
		partron,i2c-pull-up;
		Partron,vdd_ana_supply_min = <2850000>;
		Partron,vdd_ana_supply_max = <3300000>;
		Partron,vdd_ana_load_ua = <15000>;
		Partron,vddio_dig_supply_min = <1800000>;
		Partron,vddio_dig_supply_max = <1800000>;
		Partron,vddio_dig_load_ua = <10000>;
		Partron,vddio_i2c_supply_min = <1800000>;
		Partron,vddio_i2c_supply_max = <1800000>;
		Partron,vddio_i2c_load_ua = <10000>;
		Partron,near_offset = <100>;
		Partron,far_offset = <55>;
		Partron,crosstalk_max = <650>;
		Partron,ppcount = <50>;
		Partron,ps_led_current = <5>; /* 4 : 50mA 	5 : 75mA  	6 : 100mA */
	};

	/* Acceleration sensor */
	st_k303b_acc@1d {
		compatible = "st,k303b_acc";
		reg = <0x1d>;
		interrupt-parent = <&msm_gpio>;
		interrupts = <115 0x2>;
		st,vdd_ana-supply = <&pm8916_l17_sns>;
		st,vddio_i2c-supply = <&pm8916_l6>;
		pinctrl-names = "default", "sleep";
		pinctrl-0 = <&st_k303b_acc_int_active>;
		pinctrl-1 = <&st_k303b_acc_int_suspend>;
		st,irq-gpio = <&msm_gpio 115 0x00>;
		st,i2c-pull-up;
		st,axis_map_x = <1>;
		st,axis_map_y = <0>;
		st,axis_map_z = <2>;
		st,negate_x = <1>;
		st,negate_y = <1>;
		st,negate_z = <0>;
		st,poll_interval = <100>;
		st,min_interval = <2>;
		st,fs_range = <32>;
	};

	/* Magnetic Sensor */
	st_k303b_mag@1e {
		compatible = "st,k303b_mag";
		reg = <0x1e>;
		interrupt-parent = <&msm_gpio>;
		interrupts = <69 0x2>;
		st,vdd_ana-supply = <&pm8916_l17_sns>;
		st,vddio_i2c-supply = <&pm8916_l6>;
		pinctrl-names = "default", "sleep";
		pinctrl-0 = <&st_k303b_mag_active>;
		pinctrl-1 = <&st_k303b_mag_suspend>;
		st,irq-gpio = <&msm_gpio 69 0x00>;
		st,i2c-pull-up;
		st,axis_map_x = <1>;
		st,axis_map_y = <0>;
		st,axis_map_z = <2>;
		st,negate_x = <1>;
		st,negate_y = <1>;
		st,negate_z = <0>;
		st,poll_interval = <100>;
		st,min_interval = <13>;
		st,fs_range = <96>;
	};

};
